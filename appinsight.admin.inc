<?php

function appinsight_settings_form() {
  $config = _appinsight_get_config();
  $form['appinsight'] = array(
    '#type' => 'fieldset',
    '#title' => t('AppInsight Configuration'),
  );
  $form['appinsight']['appinsight_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#description' => t('Description.'),
    '#default_value' => $config['domain'],
    '#required' => TRUE,
  );
  $form['appinsight']['appinsight_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#description' => t('Description.'),
    '#default_value' => $config['private_key'],
    '#required' => TRUE,
  );
  $form['appinsight']['appinsight_enabled'] = array(
    '#title' => 'Enabled',
    '#type' => 'checkbox',
    '#description' => t('Whether or not the agent should send data back to AppInsight. (Configurable so you can disable in testing environments.)'),
    '#default_value' => $config['enabled']
  );
  return system_settings_form($form);
}